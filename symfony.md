# Symfony v5

By default Symfony starts as a micro-framework.

## Installation

1. Install PHP (>= 7.2.5)
2. Install Composer
3. Install Symfony CLI
   `$ wget https://get.symfony.com/cli/installer -O - | bash`

### Install using Symfony CLI
1. `$ symfony new my-project` (minimal)
2. `$ symfony new --full my-project` (full)

Check: `$ symfony check:requirement`

### Install using Composer
1. `$ composer create-project symfony/skeleton my-project` (minimal)
2. `$ composer create-project symfony/website-skeleton my-project` (full)

Check:
a) `$ composer require symfony/requirements-checker` (install)
b) `$ php -S 127.0.0.1:8000 -t public`
c) http://localhost:8080/check.php
d) `$ composer remove symfony/requirements-checker` (remove)

## Symfony Flex

https://symfony.sh

Examples of installing Symfony packages:
- `$ symfony composer req twig`
- `$ symfony composer req orm` (doctrine)
- `$ symfony composer req maker`

## Routes

1. routes.yaml
2. Annotations
    - `$ composer req maker`
    - `$ bin/console make:controller DefaultController`

Examples:

```php
/**
 * Simple route
 *
 * @Route("/path/", name="route_name")
 */
 public function routeA() {}

/*
 * "param" can only be digits 
 * 
 * @Route("/path/{param}", name="route_name", requirements={"param":"\d+"})
 */
 public function routeB($param) {}

/**
 * Optional
 *
 * @Route("/path/{param?}", name="route_name")
 */
 public function routeC($param) {}

/**
 * Defaults
 *
 * @Route(
 *    name="route_name",
 *    "/articles/{locale}/{year}/{slug}/{category}",
 *    defaults={"category":"computers"},
 *    requirements={"locale":"en|fr","category":"computers|smarttv|tablets", "year":"\d+"}
 * )
 */
 public function routeD($locale, $year, $slug, $category) {}

/**
 * Multiple routes
 *
 * @Route({"en":"about-us","fr":"a-propos"}, name="route_name")
 */
 public function routeE() {}
```

## Entities / Migration

- `$ symfony composer req orm` (doctrine)
- Modify DB params in .env file
- `$ bin/console make:entity`
- `$ bin/console make:migration`
- `$ bin/console doctrine:migrations:migrate`

## Services

- Simply, **services** are PHP classes that do something useful
- **Service Container**: PHP class that holds instances of other PHP classes (mailer class, db class, etc)
- Create a php class file in the `Services` folder with the proper namespace
    - src/Services/ServiceName.php
    - Namespace: app/Services
- How to get a service in our controller?
    ```php
    public function index(ServiceName $service) { }
    ```
- See auto-wiring (https://symfony.com/doc/current/service_container/autowiring.html)

## Flash messages

- Controller:
    ```php
    $this->addFlash('notice', 'message');
    ```
- View
    ```html
    {% for message in app.flashes('notice') %}
        <div class="flash-notice">{{ message }}</div>
    {% endfor %}

    {% for label, messages in app.flashes %}
        {% for message in messages %}
            <div class="flash-{{ label }}">{{ message }}</div>
        {% endfor %}
    {% endfor %}
    ```

## Request Object

```php
// Query string
$request->query->get->('page', 'default_value');

// AJAX?
$request->isXmlHttpRequest();

// Files
$request->files->get('foo');
```

## Cookies

```php
use Symfony\Component\HttpFoundation\Cookie;

$cookie = new Cookie('name-of-cookie', 'value', $expiry_in_sec);
$res    = new Response();
$res->headers->setCookie($cookie);
$res->send();
```

```php
use Symfony\Component\HttpFoundation\Request;

// Get cookie
echo $request->cookies->get('name-of-cookie');
```

## Sessions

```php
use Symfony\Component\HttpFoundation\Session\SessionInterface;

public function index(SessionInterface $sess)
{
    $sess->set('sess-name', 'sess-value');
    if ($ses->has('sess-name')) {
        echo $sess->get('sess-name');
    }

    $sess->remove();
    $sess-clear();
}
```

## Custom error pages

- src/templates/bundles/TwigBundle/Exception/error404.html.twig
- src/templates/bundles/TwigBundle/Exception/error500.html.twig

## Exceptions

```php
throw $this->createNotFoundException('user not found');
```

## Binding Services to Controllers

- services.yaml
    ```yaml
    App\Controller\DefaultController:
      bind:
        $logger: '@monolog.logger.doctrine'
    ```
- the "@" denotes a service (not a string as value)
- DefaultController.php
    ```php
    private $logger;

    public function __construct($logger)
    {
        $this->logger = $logger;
    }
    ```

## URL generator

In the controller method: `$this->generateUrl('route_name', ['param' => $value]);`

## Get values from services.yaml

- services.yaml
    ```yaml
    parameters:
        download_directory: '../path-to-some-folder'
    ```
- Controller
    ```php
    $path = $this->getParameter('download_directory');
    return $this->file($path . '/file.pdf');
    ```

## Twig

- To use assets, `$ sympony composer req symfony/asset`
- Global variables in views: in twig.yaml
    ```yaml
    twig:
        globals:
            ga_code: 'my value'
    ```
- In twig template
    ```html
    <!-- Global variable -->
    {{ ga_code }}

    <!-- Relative path -->
    {{ path('route_name') }}
    
    <!-- Absolute path -->
    {{ url('route_name') }}

    <!-- Assets in public folder - relative path -->
    {{ asset('images/logo.png') }}

    <!-- Assets in public folder - absolute path -->
    {{ absolute_url(asset('images/logo.png')) }}
    ```

## Webpack Encore

- @see https://symfony.com/doc/current/frontend.html

- `$ npm init` (creates package.json)

- `$ yarn add @symfony/webpack-encore --dev` (yarn)

- `$ npm install @symfony/webpack-encore --save-dev` (npm)

-  `$ symfony composer require symfony/webpack-encore-bundle`: This will enable
   the WebpackEncoreBundle, create the assets/ directory, add a webpack.config.js
   file, and add node_modules/ to .gitignore.

- `$ npm install`

- Edit **webpack.config.js** and add the following:
    ```js
    .enableVueLoader()
    .addEntry('app', './assets/vuejs/index.js')
    ```

- `npm install vue vue-loader vue-template-compiler --save-dev`

- In `assets` folder, create the following files in subfolder `vuejs`:
    - App.vue
    - index.js

- Contents of `App.vue`:
    ```vue
    <template>
    <div class="vue-js" style="margin:60px 0">
        <h1>Hello from VueJS</h1>
        <button @click="count=count+1">CLICK HERE! COUNT = {{ count }}</button>
        <hr />
    </div>
    </template>
    <script>
    export default {
        name: "App",
        data: function () {
            return {
                count: 0
            }
        }
    };
    </script>
    ```

- Contents of `index.js`:
    ```js
    import Vue from "vue";
    import App from "./App";

    new Vue({
        components: { App },
        template: "<App />"
    })
    .$mount("#VuejsApp");
    ```

## Tools to adhere to coding standards

- https://packagist.org/packages/doctrine/coding-standard
- https://packagist.org/packages/squizlabs/php_codesniffer
- https://packagist.org/packages/phpstan/phpstan
- https://packagist.org/packages/phpstan/phpstan-symfony
- https://packagist.org/packages/thecodingmachine/phpstan-strict-rules
- https://packagist.org/packages/thecodingmachine/phpstan-safe-rule
- https://github.com/thecodingmachine/safe
- https://github.com/Roave/SecurityAdvisories
