<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private ObjectManager $om;
    private UserPasswordHasherInterface $passwordEncoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->passwordEncoder = $encoder;
    }

    final public function load(ObjectManager $manager): void
    {
        $this->om = $manager;
        $this->loadUsers();
        $manager->flush();
    }

    private function loadUsers(): void
    {
        $users = [
            ['email' => 'super-admin@symfony.apache', 'role' => User::ROLE_SUPER_ADMIN],
            ['email' => 'admin@symfony.apache',       'role' => User::ROLE_ADMIN],
            ['email' => 'user@symfony.apache',        'role' => User::ROLE_USER],
            ['email' => 'developer@symfony.apache',   'role' => User::ROLE_DEVELOPER],
        ];

        foreach ($users as $user) {
            $User = (new User())
                ->setEmail($user['email'])
                ->setRoles([$user['role']])
                ->setIsVerified(true)
            ;
            $User->setPassword($this
                ->passwordEncoder
                ->hashPassword(
                    $User,
                    'nadim'
                )
            );
            $this->om->persist($User);
        }
    }
}
